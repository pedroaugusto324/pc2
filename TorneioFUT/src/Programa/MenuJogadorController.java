package Programa;

import Controller.Controle;
import Model.Jogador;
import Model.OpcaoMenu;
import View.JogadorView;
import View.MenuView;
import Controller.ContratoControle;

public class MenuJogadorController {

    private OpcaoMenu[] opcoes;
    private MenuView menu;

    public MenuJogadorController(ContratoControle cc) {
        OpcaoMenu[] opcoes = {OpcaoMenu.JOGADORCRIAR,
            OpcaoMenu.JOGADORLISTAR,
            OpcaoMenu.DELETARJOGADOR,
            OpcaoMenu.ATUALIZARJOGADOR,
            OpcaoMenu.SAIR};
        menu = new MenuView(opcoes);
        this.cc = cc;
    }
    ContratoControle cc;
    Controle<Jogador> controle = new Controle<Jogador>(new JogadorView<Jogador>());

    public void mostraMenu(OpcaoMenu escolha) {

        do {

            escolha = menu.getOpcao();

            switch (escolha) {
                case JOGADORCRIAR:
                    cc.Contratar(controle.create());
                    break;
                case JOGADORLISTAR:
                    controle.listar();
                    break;
                case DELETARJOGADOR:
                    controle.delete();
                    break;
                case ATUALIZARJOGADOR:
                    controle.update();
            }

        } while (escolha != OpcaoMenu.SAIR);
    }
}
