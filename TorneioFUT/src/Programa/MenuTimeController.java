package Programa;

import Controller.Controle;
import Model.OpcaoMenu;
import Model.Time;
import View.MenuView;
import View.TimeView;

public class MenuTimeController {

    private OpcaoMenu[] opcoes;
    private MenuView menu;

    public MenuTimeController() {
        OpcaoMenu[] opcoes = {OpcaoMenu.TIMECRIAR,
            OpcaoMenu.TIMELISTAR,
            OpcaoMenu.DELETARTIME,
            OpcaoMenu.SAIR};
        menu = new MenuView(opcoes);

    }
    Controle<Time> controle = new Controle<Time>(new TimeView<Time>());

    public void mostraMenu(OpcaoMenu escolha) {

        do {

            escolha = menu.getOpcao();

            switch (escolha) {
                case TIMECRIAR:
                    controle.create();
                    break;
                case TIMELISTAR:
                    controle.listar();
                    break;
                case DELETARTIME:
                    controle.delete();
                    break;
                case PESQUISAR:
                    controle.findById();
            }

        } while (escolha != OpcaoMenu.SAIR);
    }

    public Controle<Time> getControle() {
        return controle;
    }

}
