package Programa;

import Controller.ContratoControle;
import Model.OpcaoMenu;
import View.MenuView;

public class ProgramaCadastro {

    public static void main(String[] args) {

        System.out.println("TORNEIO DE FUTEBOL - CADASTRO");

        OpcaoMenu[] opcoes = {OpcaoMenu.TIME,
            OpcaoMenu.JOGADOR,
            OpcaoMenu.SAIR};

        MenuView menu = new MenuView(opcoes);
        MenuTimeController time = new MenuTimeController();
        ContratoControle cc = new ContratoControle(time.getControle());
        MenuJogadorController jogador = new MenuJogadorController(cc);
        OpcaoMenu escolha;

        do {

            escolha = menu.getOpcao();

            switch (escolha) {
                case JOGADOR:
                    jogador.mostraMenu(escolha);
                    break;
                case TIME:
                    time.mostraMenu(escolha);
                    break;
            }
        } while (escolha != OpcaoMenu.SAIR);

    }
}
