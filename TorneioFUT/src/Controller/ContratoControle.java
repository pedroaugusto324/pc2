package Controller;

import Model.Jogador;
import Model.Time;
import Controller.Controle;
import Model.IModel;
import java.util.List;

public class ContratoControle {

    Controle<Time> ctime;

    public ContratoControle(Controle<Time> time) {
        this.ctime = time;
    }

    public List<Jogador> Contratar(Jogador j) {
        Time temp = ctime.findById();
        if ((temp != null) && (j != null)) {
            temp.contratar(j);
            return temp.getPlantel();
        } else {
            return null;
        }
    }
}
