package Controller;

import DAO.IDAO;
import DAO.GenericDAO;
import Model.IModel;
import View.IViewCrud;

public class Controle<T extends IModel> implements IControle<T> {

    private IDAO<T> dao;
    private IViewCrud<T> tela;

    public Controle(IViewCrud<T> tela) {
        dao = new GenericDAO();
        this.tela = tela;
    }

    @Override
    public T create() {
        T t = tela.create();
        dao.create(t);
        return t;
    }

    @Override
    public void delete() {
        long id = tela.askForInt("Digite o id: ");
        T x = dao.findById(id);
        dao.delete(x);
    }

    @Override
    public void listar() {
        tela.list(dao.findAll());
    }

    @Override
    public void update() {
        long id = tela.askForInt("Digite o id: ");
        T j = dao.findById(id);
        tela.update(j);
    }

    @Override
    public T findById() {
        long id = tela.askForInt("Digite o id: ");
        return dao.findById(id);
    }

}
