package Controller;

public interface IControle<T> {

    public T create();
    
    public void delete();

    public void listar();
    
    public void update();
    
    public T findById();
    

}