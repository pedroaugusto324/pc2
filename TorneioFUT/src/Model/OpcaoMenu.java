package Model;

public enum OpcaoMenu {
    TIME("Times"),
    JOGADOR("Jogadores"),
    TIMECRIAR("Criar Times"),
    TIMELISTAR("Listar Times"),
    DELETARTIME("Deletar Times"),
    JOGADORCRIAR("Cadastrar Jogadores"),
    JOGADORLISTAR("Listar Jogadores"),
    DELETARJOGADOR("Deletar Jogadores"),
    ATUALIZARJOGADOR("Atualizar Jogadores"),
    CONTRATAR("Contratar Jogadores"),
    PESQUISAR("Pesquisa por id"),
    SAIR("Sair");

    private String opcao;

    OpcaoMenu(String opcao) {
        this.opcao = opcao;
    }

    public String getValue() {
        return opcao;
    }

}
