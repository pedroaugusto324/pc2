package Model;

import java.io.Serializable;
import Model.Jogador;
import java.util.ArrayList;
import Model.Jogador;

public class Time implements Serializable, IModel {

    private static final long serialVersionUID = 1L;
    private static long gerador = 1;
    private Long id;
    private String nome;
    private ArrayList<Jogador> plantel;

    public Time(Long id) {
        this.id = id;

    }

    public Time(String nome) {
        plantel = new ArrayList();
        this.id = gerador;
        this.nome = nome;
        gerador++;
    }

    public void contratar(Jogador j) {
        if (j != null) {
            plantel.add(j);
        }
    }

    public ArrayList<Jogador> getPlantel() {
        return plantel;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Time)) {
            return false;
        }

        Time other = (Time) object;

        if ((this.nome == null && other.nome != null) || (this.nome != null && !this.nome.equals(other.nome))) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return "Id: " + this.id + " Nome do Time: " + this.nome + "\n" + plantel.toString();

    }
}
