package Model;

import java.io.Serializable;
import java.util.List;

public class Jogador implements Serializable, IModel {

    private static final long serialVersionUID = 1L;
    private static long gerador = 1;
    private Long id;
    private String nome;
    private int idade;
    private Long idtime;

    public Jogador(Long id) {
        this.id = id;
    }

    public Jogador(String nome) {
        this.id = gerador;
        this.nome = nome;
        gerador++;
    }

    public Jogador(String nome, int idade) {
        this.id = gerador;
        this.nome = nome;
        this.idade = idade;
        gerador++;
    }

    public int getIdade() {
        return idade;
    }
    
    public Long getIdTime(){
        return idtime;
    }

    public boolean setIdade(int idade) {
        this.idade = idade;
        return true;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jogador)) {
            return false;
        }
        Jogador other = (Jogador) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Id: " + this.id + " Nome: " + this.nome + " Idade: " + this.idade;
    }
}
