package Model;

public interface IModel<T> {

    public Long getId();
    public void setNome(String nome);
    public String getNome();
    public String toString();
}
