package DAO;

import java.util.List;
import java.util.ArrayList;

public interface IDAO<T> {

    public void create(T t);

    public boolean delete(T t);

    public T update(T t);

    public T findById(Long id);

    public List<T> findAll();
}
