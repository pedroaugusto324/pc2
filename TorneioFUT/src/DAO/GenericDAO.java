package DAO;

import java.util.List;
import Model.IModel;
import java.util.ArrayList;

public class GenericDAO<T extends IModel> implements IDAO<T> {

    private ArrayList<T> lista;

    public GenericDAO() {
        lista = new ArrayList<T>();
    }

    @Override
    public void create(T t) {
        if (t != null) {
            lista.add(t);
        }
    }

    @Override
    public boolean delete(T t) {
        if (t != null) {
            if (lista.contains(t) == true) {
                lista.remove(t);
                return true;
            }
        }
        return false;
    }

    @Override
    public T update(T t) {
        return null;
    }

    @Override
    public T findById(Long id) {
        if (id != null) {
            for (int i = 0; i < lista.size(); i++) {
                if (lista.get(i).getId() == id) {
                    return lista.get(i);
                }
            }
        }
        return null;
    }

    @Override
    public ArrayList<T> findAll() {
        return lista;
    }

}
