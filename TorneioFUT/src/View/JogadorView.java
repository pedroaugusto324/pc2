package View;

import Model.Jogador;
import Model.Time;
import java.util.ArrayList;
import View.ViewSimples;
import View.IViewCrud;
import java.util.List;

public class JogadorView<T extends Jogador> extends ViewSimples implements IViewCrud<T> {

    @Override
    public T create() {
        T t;
        this.showMessage("Forneça o Nome do Jogador:");
        String nome = (entrada.next());

        this.showMessage("Forneça a idade do jogador: (Somente Digitos)");
        int idade = (entrada.nextInt());
        while (idade <= 0) {
            this.showMessage("Forneça a idade do jogador: (Somente Digitos)");
            idade = (entrada.nextInt());
        }
        this.showMessage("Escolha o time: ");
        
        t = (T) new Jogador(nome, idade);
        return t;
    }

    @Override
    public void update(Jogador j) {

        this.showMessage("Forneça o Nome do Jogador:");
        j.setNome(entrada.next());

        this.showMessage("Forneça a idade do jogador: (Somente Digitos)");
        j.setIdade(entrada.nextInt());

        while (j.getIdade() <= 0) {
            this.showMessage("Forneça a idade do jogador: (Somente Digitos)");
            j.setIdade(entrada.nextInt());
        }
    }

    @Override
    public void read(Jogador j) {
        this.showMessage("Mostrar Jogador");
        this.showMessage(j.toString());
    }
    
    @Override
    public void list(List<T> t) {
        System.out.println("Listagem: ");
        for (int i = 0; i < t.size(); i++) {
            System.out.println(t.get(i).toString());
        }
        System.out.println("");

    }
}
