package View;

import Model.Time;
import java.util.ArrayList;
import java.util.List;

public class TimeView<T extends Time> extends ViewSimples implements IViewCrud<T> {

    public T create() {
        T t;
        this.showMessage("Forneça o Nome do Time:");
        String nome = (entrada.next());

        t = (T) new Time(nome);
        return t;
    }

    public void update(Time t) {
        this.showMessage("Forneça o Nome do Time:");
        t.setNome(entrada.next());
    }

    @Override
    public void read(Time t) {
        this.showMessage("Mostrar Time");
        this.showMessage(t.toString());
    }

    @Override
    public void list(List<T> t) {
        System.out.println("Listagem: ");
        for (int i = 0; i < t.size(); i++) {
            System.out.println(t.get(i).toString());
        }
        System.out.println("");

    }
}
