package View;

import Model.IModel;
import java.util.List;

public interface IViewCrud<T extends IModel> extends IBasicView {

    public T create();

    public void read(T t);

    public void update(T t);
    
    public void list(List<T> t);

}
