package View;

import Model.OpcaoMenu;

public class MenuView extends ViewSimples {

    OpcaoMenu opcoes[];

    public MenuView(OpcaoMenu[] opcoes) {
        this.opcoes = opcoes;
    }

    public OpcaoMenu getOpcao() {

        OpcaoMenu escolha;
        int numero;
        do {
            for (int i = 0; i < opcoes.length; i++) {
                showMessage(i + " - " + opcoes[i].getValue());
            }
            numero = askForInt("Escolha uma Opção");

        } while (numero < 0 || numero >= opcoes.length);

        escolha = opcoes[numero];

        return escolha;

    }

}
