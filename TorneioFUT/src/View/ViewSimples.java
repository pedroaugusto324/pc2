package View;

import Model.Jogador;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ViewSimples implements IBasicView {

    protected Scanner entrada;

    public ViewSimples() {
        entrada = new Scanner(System.in);
    }

    @Override
    public void showMessage(String msg) {
        System.out.println(msg);
    }

    @Override
    public void showErrorMessage(String msg) {
        System.out.println(msg);
    }

    @Override
    public int askForInt(String question) {
        int inteiro = 0;
        boolean erro = true;

        while (erro) {
            try {
                showMessage(question);
                inteiro = entrada.nextInt();
                erro = false;
            } catch (InputMismatchException e) {
                this.showErrorMessage("Por favor insira apenas números!!");
                erro = true;
                entrada.next();
            }
        }
        return inteiro;

    }

    @Override
    public double askForDouble(String question) {
        showMessage(question);
        double d = entrada.nextDouble();
        return d;

    }

    @Override
    public String askForString(String question) {
        showMessage(question);
        return entrada.next();
    }

}
